package init.app.web.dto;

public class ProcessModelDTO {
    private String processId;
    private String tempUser;

    public ProcessModelDTO(String processId, String tempUser) {
        this.processId = processId;
        this.tempUser = tempUser;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getTempUser() {
        return tempUser;
    }

    public void setTempUser(String tempUser) {
        this.tempUser = tempUser;
    }
}
