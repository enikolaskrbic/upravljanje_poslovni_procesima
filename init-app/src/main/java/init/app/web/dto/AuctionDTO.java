package init.app.web.dto;

import java.io.Serializable;

public class AuctionDTO implements Serializable{
    String korisnik;

    public String getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(String korisnik) {
        this.korisnik = korisnik;
    }
}
