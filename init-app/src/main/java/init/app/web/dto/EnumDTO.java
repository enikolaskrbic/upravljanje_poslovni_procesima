package init.app.web.dto;

/**
 * Created by Bakir Niksic on 2/11/2018.
 */
public class EnumDTO {

    private String name;
    private String value;

    public EnumDTO(){

    }

    public EnumDTO(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
