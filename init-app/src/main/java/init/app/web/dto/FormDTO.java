package init.app.web.dto;

import java.util.ArrayList;

/**
 * Created by Bakir Niksic on 2/11/2018.
 */
public class FormDTO {

    private String type;
    private String name;
    private String label;
    private String value;
    private boolean required;
    private boolean writable;
    private boolean readable;
    private ArrayList<EnumDTO> values;

    public FormDTO(){

    }

    public FormDTO(String type, String name, String label, String value, boolean required, boolean writable, boolean readable, ArrayList<EnumDTO> values) {
        this.type = type;
        this.name = name;
        this.label = label;
        this.value = value;
        this.required = required;
        this.writable = writable;
        this.readable = readable;
        this.values = values;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isWritable() {
        return writable;
    }

    public void setWritable(boolean writable) {
        this.writable = writable;
    }

    public boolean isReadable() {
        return readable;
    }

    public void setReadable(boolean readable) {
        this.readable = readable;
    }

    public ArrayList<EnumDTO> getValues() {
        return values;
    }

    public void setValues(ArrayList<EnumDTO> values) {
        this.values = values;
    }

}
