package init.app.controller;

import init.app.service.TaskService;
import init.app.web.dto.FormDTO;
import init.app.web.dto.TaskModelDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/tasks/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<TaskModelDTO>> getUserTasks(@PathVariable("userId") String userId){
        return new ResponseEntity<List<TaskModelDTO>>(taskService.getAllTasks(userId), HttpStatus.OK);
    }

    @CrossOrigin(origins="*")
    @RequestMapping(value = "/tasks/form/{taskId}", method = RequestMethod.GET)
    public ResponseEntity<List<FormDTO>> getFormProperties(@PathVariable("taskId") String taskId){
        return new ResponseEntity<List<FormDTO>>(taskService.getFormPropeties(taskId), HttpStatus.OK);
    }
//
    @CrossOrigin(origins="*")
    @RequestMapping(value = "/tasks/form/{taskId}", method = RequestMethod.POST)
    public ResponseEntity<List<FormDTO>> postFormPropetiesOnTaskAndExecute(@PathVariable("taskId") String taskId, @RequestBody List<FormDTO> myFormProperties) {
        if (myFormProperties == null) {
            return new ResponseEntity<List<FormDTO>>(HttpStatus.BAD_REQUEST);
        }
        taskService.postFormPropetiesOnTaskAndExecute(taskId, myFormProperties);

        return new ResponseEntity<List<FormDTO>>(myFormProperties, HttpStatus.OK);
    }
}
