package init.app.controller;


import init.app.entities.Korisnik;
import init.app.exception.CustomException;
import init.app.helper.TaskHelper;
import init.app.service.KategorijaService;
import init.app.service.KorisnikService;
import init.app.service.activiti.AukcijaServis;
import init.app.service.activiti.RegistracijaServis;
import init.app.web.dto.*;
import org.activiti.engine.FormService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class StartController {

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    KorisnikService korisnikService;

    @Autowired
    KategorijaService kategorijaService;

    @Autowired
    FormService formService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    @Autowired
    private TaskHelper taskHelper;

    @RequestMapping(value = "/start-register", method = RequestMethod.POST)
    public ResponseEntity startService() {
        String regKorisnikRand = UUID.randomUUID().toString();
        String logKorisnikRand = UUID.randomUUID().toString();
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("registracijaServis", new RegistracijaServis());
        variables.put("maxUdaljenost", 0L);
        variables.put("regKorisnik", "regKorisnik" + regKorisnikRand);
        variables.put("logKorisnik", "logKorisnik" + logKorisnikRand);

        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("registrationProcess", variables);

        return new ResponseEntity<>(new ProcessModelDTO(processInstance.getId(), "regKorisnik" + regKorisnikRand), HttpStatus.OK);
    }

    @RequestMapping(value = "/aukcija", method = RequestMethod.POST)
    public ResponseEntity registerUser(@RequestBody AuctionDTO korisnik) throws CustomException {

        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("aukcijaServis", new AukcijaServis());
        variables.put("korisnik", korisnik.getKorisnik());
        variables.put("brojPonavljanjaProcesa", 0L);
        variables.put("ponudaId", -1L);
        variables.put("zahtevId", -1L);
        variables.put("pronadjenaKompanija",-1L);
        Korisnik logKorisnik;
        try {
            logKorisnik = korisnikService.findByUsername(korisnik.getKorisnik()).get(0);
        } catch (Exception e) {
            throw new CustomException("User doesn't exists!");
        }
        for (Task task : taskService.createTaskQuery().taskAssignee(korisnik.getKorisnik()).list()) {
            if(task.getProcessDefinitionId().contains("aukcijaProcess")){
                return new ResponseEntity<>(logKorisnik, HttpStatus.OK);
            }
        }
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("aukcijaProcess", variables);

        return new ResponseEntity<>(logKorisnik, HttpStatus.OK);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity registerUser(@RequestBody LoginDTO loginDTO) throws CustomException {
        Korisnik korisnik;
        if(korisnikService.doesUserExist(loginDTO.getKorisnickoIme(),loginDTO.getLozinka())){
            korisnik = korisnikService.findByUsername(loginDTO.getKorisnickoIme()).get(0);
            return new ResponseEntity<>(korisnik, HttpStatus.OK);
        }else{
            ErrorDTO errorDTO = new ErrorDTO();
            errorDTO.setMessage("User doesn't exists!");
            errorDTO.setStatus(HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public ResponseEntity getCategory() throws CustomException {
        return new ResponseEntity<>(kategorijaService.findAll().stream().filter(x->x.getKorisnickoIme()==null).collect(Collectors.toList()), HttpStatus.OK);
    }


}
