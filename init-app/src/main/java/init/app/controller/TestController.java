package init.app.controller;

import init.app.component.StaticService;
import init.app.service.activiti.AukcijaServis;
import init.app.service.activiti.RegistracijaServis;
import init.app.web.dto.ProcessModelDTO;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api")
public class TestController {

    @Inject
    AukcijaServis aukcijaServis;

    @RequestMapping(value = "/sacuvajZahtev", method = RequestMethod.GET)
    public ResponseEntity sacuvajZahtev(@RequestParam long zahtevId,
                                        @RequestParam String kategorija_posla,
                                        @RequestParam String opis_posla,
                                        @RequestParam long max_procenjena_vrednost,
                                        @RequestParam String rok_za_prijem_ponude,
                                        @RequestParam long max_br_ponuda,
                                        @RequestParam String rok_za_izvrsenje_usluga) {
        long zahevId = aukcijaServis.sacuvajZahtev(zahtevId,kategorija_posla, opis_posla, max_procenjena_vrednost, rok_za_prijem_ponude, max_br_ponuda, rok_za_izvrsenje_usluga);
        return new ResponseEntity<>(zahevId, HttpStatus.OK);
    }

    @RequestMapping(value = "/listaKompanija", method = RequestMethod.GET)
    public ResponseEntity listaKompanija(@RequestParam String kategorija_posla,
                                         @RequestParam String korisnickoIme) {
        return new ResponseEntity<>(aukcijaServis.listaKompanija(kategorija_posla, korisnickoIme), HttpStatus.OK);
    }

    @RequestMapping(value = "/sacuvajPonudu", method = RequestMethod.GET)
    public ResponseEntity sacuvajPonudu(@RequestParam long ponudaId,
                                        @RequestParam long zahtevId,
                                        @RequestParam String korisnickoIme,
                                        @RequestParam long cena,
                                        @RequestParam String datumPonude) {
        return new ResponseEntity<>(aukcijaServis.sacuvajPonudu(ponudaId, zahtevId, korisnickoIme, cena, datumPonude), HttpStatus.OK);
    }
}
