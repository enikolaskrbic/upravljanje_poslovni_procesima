package init.app.controller;

import init.app.component.StaticService;
import init.app.entities.Kategorija;
import init.app.entities.Kordinate;
import init.app.entities.Korisnik;
import init.app.service.*;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Controller;

@Controller
public class InitController implements CommandLineRunner {

    private static final String filename = "diagrams/RegistrationProcess.bpmn";
    private static final String groupsPath ="./src/main/resources/properties/groups.yml";
    private static final String usersPath ="./src/main/resources/properties/users.yml";

    @Autowired
    public InitController(EmailService emailService,
                          KategorijaService kategorijaService,
                          KorisnikService korisnikService,
                          KordinateService kordinateService,
                          ZahtevServis zahtevServis,
                          PonudaService ponudaService,
                          OcenaServis ocenaServis,
                          TaskService taskService){
        StaticService.emailService = emailService;
        StaticService.kordinateService = kordinateService;
        StaticService.kategorijaService = kategorijaService;
        StaticService.korisnikService = korisnikService;
        StaticService.zahtevServis = zahtevServis;
        StaticService.ponudaService = ponudaService;
        StaticService.ocenaServis = ocenaServis;
        StaticService.taskService = taskService;
    }

    @Autowired
    KategorijaService kategorijaService;

    @Autowired
    KorisnikService korisnikService;

    @Autowired
    KordinateService kordinateService;


    @Override
    public void run(String... strings) throws Exception {
        initCategories();
    }

    private void deploy(){
        RepositoryService repositoryService = ProcessEngines.getDefaultProcessEngine().getRepositoryService();
        for (Deployment d : repositoryService.createDeploymentQuery().list())
        {
            repositoryService.deleteDeployment(d.getId(), true);
        }
        repositoryService.createDeployment().addClasspathResource("processes/reg.bpmn").deploy();
        repositoryService.createDeployment().addClasspathResource("processes/aukcija.bpmn").deploy();
    }

    private void initCategories() {
        Kategorija kategorija = new Kategorija();
        kategorija.setIme("K1");
        kategorija.setActivitiId("k1");
        kategorija.setOpis("opis k1");

        Kategorija kategorija2 = new Kategorija();
        kategorija2.setIme("K2");
        kategorija2.setActivitiId("k2");
        kategorija2.setOpis("opis k2");

        Kategorija kategorija3 = new Kategorija();
        kategorija3.setIme("K3");
        kategorija3.setActivitiId("k3");
        kategorija3.setOpis("opis k3");

        kategorijaService.save(kategorija);
        kategorijaService.save(kategorija2);
        kategorijaService.save(kategorija3);

        Korisnik k = new Korisnik();
        k.setAdresa("");
        k.setEmail("enikolaskrbic@gmail.com");
        k.setGrad("Novi Sad");
        k.setIme("Osoba");
        k.setKorisnickoIme("osoba");
        k.setLozinka("123456");
        k.setMaxUdaljenost(100);
        k.setPostanskiBroj("21000");
        k.setTip("osoba");
        k.setVerifikovan(true);
        k.setxKordinata(45);
        k.setyKordinata(19);

        Korisnik k2 = new Korisnik();
        k2.setAdresa("");
        k2.setEmail("eivansavic@gmail.com");
        k2.setGrad("Novi Sad");
        k2.setIme("Kompanija");
        k2.setKorisnickoIme("kompanija");
        k2.setLozinka("123456");
        k2.setMaxUdaljenost(100);
        k2.setPostanskiBroj("21000");
        k2.setTip("kompanija");
        k2.setVerifikovan(true);
        k2.setxKordinata(45);
        k2.setyKordinata(19);

        Kategorija kat = new Kategorija();
        kat.setIme("K1");
        kat.setActivitiId("k1");
        kat.setKorisnickoIme("kompanija");
        kat.setOpis("opis k1");

        kategorijaService.save(kat);

        korisnikService.save(k);
        korisnikService.save(k2);

        Kordinate kordinate = new Kordinate();
        kordinate.setxKordinata(45);
        kordinate.setyKordinata(19);
        kordinate.setGrad("Novi sad");
        kordinate.setKorisnickoIme("osoba");

        Kordinate kordinate2 = new Kordinate();
        kordinate2.setxKordinata(45);
        kordinate2.setyKordinata(19);
        kordinate2.setGrad("Novi sad");
        kordinate2.setKorisnickoIme("kompanija");

        kordinateService.save(kordinate);
        kordinateService.save(kordinate2);



    }
}
