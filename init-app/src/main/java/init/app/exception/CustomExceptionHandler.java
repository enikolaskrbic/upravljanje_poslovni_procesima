package init.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.CONFLICT;

@ControllerAdvice
public class CustomExceptionHandler {

    final static public String DEFAULT_EXCEPTION = "Oops, something went wrong!";

    @ExceptionHandler(value = {Exception.class, CustomException.class})
    public static ResponseEntity handleException(Exception exception, HttpStatus httpStatus) {
        if (exception.getClass().equals(CustomException.class)) {
            return handleCustomException((CustomException) exception, httpStatus);
        } else {
            return new ResponseEntity<>(DEFAULT_EXCEPTION, httpStatus);
        }
    }

    private static ResponseEntity handleCustomException(CustomException customException, HttpStatus httpStatus) {
        return new ResponseEntity<>(customException.getMessage(), customException.getHttpStatus() != null ? customException.getHttpStatus() : httpStatus);
    }

    public static ResponseEntity handleException(Exception exception) {
        return handleException(exception, CONFLICT);
    }
}
