package init.app.repository;

import init.app.entities.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface KorisnikRepository extends JpaRepository<Korisnik,Long> {

    @Query("select korisnik from Korisnik korisnik where korisnik.korisnickoIme = :korisnickoIme or korisnik.email = :email")
    public List<Korisnik> findByUsernameAndMail(@Param("korisnickoIme") String username, @Param("email") String email);

    @Query("select korisnik from Korisnik korisnik where korisnik.korisnickoIme = :korisnickoIme and korisnik.lozinka = :lozinka and korisnik.verifikovan = 1")
    public List<Korisnik> findByUsernameAndPassword(@Param("korisnickoIme") String username, @Param("lozinka") String password);

    @Query("select korisnik from Korisnik korisnik where korisnik.korisnickoIme = :korisnickoIme")
    public List<Korisnik> findByUsername(@Param("korisnickoIme") String username);
}