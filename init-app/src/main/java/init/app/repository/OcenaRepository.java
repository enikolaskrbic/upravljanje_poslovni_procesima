package init.app.repository;

import init.app.entities.Ocena;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OcenaRepository extends JpaRepository<Ocena,Long> {
}
