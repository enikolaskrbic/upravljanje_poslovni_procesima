package init.app.repository;

import init.app.entities.Kategorija;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KategorijaRepository extends JpaRepository<Kategorija,Long> {
    @Query("select kategorija from Kategorija kategorija where kategorija.korisnickoIme = :korisnickoIme")
    public List<Kategorija> findByUsername(@Param("korisnickoIme") String username);
}
