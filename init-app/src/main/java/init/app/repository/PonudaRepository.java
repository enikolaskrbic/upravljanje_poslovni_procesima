package init.app.repository;

import init.app.entities.Ponuda;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PonudaRepository extends JpaRepository<Ponuda,Long> {
}
