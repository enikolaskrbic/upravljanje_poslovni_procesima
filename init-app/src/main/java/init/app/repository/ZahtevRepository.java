package init.app.repository;

import init.app.entities.Zahtev;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZahtevRepository extends JpaRepository<Zahtev,Long> {
}
