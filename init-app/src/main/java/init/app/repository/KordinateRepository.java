package init.app.repository;

import init.app.entities.Kordinate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KordinateRepository extends JpaRepository<Kordinate,Long> {
    @Query("select kordinate from Kordinate kordinate where kordinate.korisnickoIme = :korisnickoIme")
    public List<Kordinate> findByUsername(@Param("korisnickoIme") String username);
}
