package init.app.util;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import static java.lang.Enum.valueOf;

public class ResultSetParserUtil {

    public static List<Object> parseResultSet(List<Object[]> resultSet, Class clazz) {

        final List<Object> objects = new ArrayList<>();

        resultSet.forEach((record) -> {
            Object newObject = null;

            try {
                final Field[] fields = clazz.getDeclaredFields();
                newObject = clazz.newInstance();

                for (int i = 0; i < fields.length; i++) {
                    Field field = fields[i];
                    field.setAccessible(true);

                    final Class fieldType = field.getType();

                    if (record.length <= i || record[i] == null) {
                        field.set(newObject, null);
                    } else if (record[i].getClass() == BigInteger.class) {
                        field.set(newObject, ((BigInteger) record[i]).longValue());
                    } else if (record[i].getClass() == Timestamp.class) {
                        field.set(newObject, ((Timestamp) record[i]).toInstant().atZone(TimeZone.getDefault().toZoneId()));
                    } else if (fieldType.isEnum()) {
                        field.set(newObject, valueOf(fieldType, (String) record[i]));
                    } else {
                        field.set(newObject, fieldType.cast(record[i]));
                    }
                }
            } catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }

            objects.add(newObject);
        });

        return objects;
    }
}
