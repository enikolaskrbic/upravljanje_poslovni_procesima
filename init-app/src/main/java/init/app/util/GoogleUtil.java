package init.app.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

public class GoogleUtil {
    public static LatLng kordinateOdGrada(String adresa, String grad, String postanskiBroj){
        String address= adresa  + ", " + grad + ", " + postanskiBroj+", Srbija";
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("AIzaSyA74dmDSIsfMCsaRN0wZkIu_qwfDBvIcHQ")
                .build();
        try {
            GeocodingResult[] results =  GeocodingApi.geocode(context, address).await();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            System.out.println(gson.toJson(results[0].geometry.location.lat));
            System.out.println(gson.toJson(results[0].geometry.location.lng));
            return results[0].geometry.location;
        }catch (Exception e){
            System.err.println("GEO COORDINATES ERROR : ");
        }

        return new LatLng(0,0);
    }
}
