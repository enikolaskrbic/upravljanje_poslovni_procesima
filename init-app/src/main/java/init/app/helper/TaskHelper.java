package init.app.helper;

import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskHelper {

    @Autowired
    private TaskService taskService;

    public Task getLastTaskByAssignee(String username){
        List<Task> tasks = findTaskByAssignee(username);
        return tasks.get(tasks.size()-1);
    }

    private List<Task> findTaskByAssignee(String username){
        try{
            List<Task> tasks = taskService.createTaskQuery().taskAssignee(username).list();
            System.out.println("ASSIGNEE tasks.size = " + tasks.size());
            for(Task t : tasks){
                System.out.println("TASK: " + t.toString());
            }
            return tasks;

        }catch (Exception e){
            return null;
        }
    }

}
