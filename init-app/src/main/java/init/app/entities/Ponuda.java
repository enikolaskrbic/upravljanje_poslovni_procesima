package init.app.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Ponuda {
    @Id
    @GeneratedValue
    private Long id;
    private long cena;
    private String datum;
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Zahtev zahtev;
    private String status;
    private String korisnickoIme;

    public Ponuda(){

    }

    public Ponuda(long cena, String datum, Zahtev zahtev, String status, String korisnickoIme) {
        this.cena = cena;
        this.datum = datum;
        this.zahtev = zahtev;
        this.status = status;
        this.korisnickoIme = korisnickoIme;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCena() {
        return cena;
    }

    public void setCena(long cena) {
        this.cena = cena;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public Zahtev getZahtev() {
        return zahtev;
    }

    public void setZahtev(Zahtev zahtev) {
        this.zahtev = zahtev;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    @Override
    public String toString() {
        return "Ponuda{" +
                "id=" + id +
                ", cena=" + cena +
                ", datum='" + datum + '\'' +
                ", status='" + status + '\'' +
                ", korisnickoIme='" + korisnickoIme + '\'' +
                '}';
    }
}
