package init.app.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Kordinate {

    @Id
    @GeneratedValue
    private Long id;
    public double xKordinata;
    public double yKordinata;
    public String grad;
    public String korisnickoIme;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getxKordinata() {
        return xKordinata;
    }

    public void setxKordinata(double xKordinata) {
        this.xKordinata = xKordinata;
    }

    public double getyKordinata() {
        return yKordinata;
    }

    public void setyKordinata(double yKordinata) {
        this.yKordinata = yKordinata;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }
}
