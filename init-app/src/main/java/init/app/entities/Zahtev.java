package init.app.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Zahtev {
    @Id
    @GeneratedValue
    private Long id;
    private String kategorija;
    private String opisPosla;
    private long maxCena;
    private String rokZaPrijemPonude;
    private long maxBrojPonuda;
    private String rokZaIzvrsenje;
    @OneToMany(mappedBy = "zahtev", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Ponuda> listaPonuda;

    public Zahtev(){

    }

    public Zahtev(String kategorija, String opisPosla, long maxCena, String rokZaPrijemPonude, long maxBrojPonuda, String rokZaIzvrsenje) {
        this.kategorija = kategorija;
        this.opisPosla = opisPosla;
        this.maxCena = maxCena;
        this.rokZaPrijemPonude = rokZaPrijemPonude;
        this.maxBrojPonuda = maxBrojPonuda;
        this.rokZaIzvrsenje = rokZaIzvrsenje;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public String getOpisPosla() {
        return opisPosla;
    }

    public void setOpisPosla(String opisPosla) {
        this.opisPosla = opisPosla;
    }

    public long getMaxCena() {
        return maxCena;
    }

    public void setMaxCena(long maxCena) {
        this.maxCena = maxCena;
    }


    public long getMaxBrojPonuda() {
        return maxBrojPonuda;
    }

    public void setMaxBrojPonuda(long maxBrojPonuda) {
        this.maxBrojPonuda = maxBrojPonuda;
    }

    public String getRokZaPrijemPonude() {
        return rokZaPrijemPonude;
    }

    public void setRokZaPrijemPonude(String rokZaPrijemPonude) {
        this.rokZaPrijemPonude = rokZaPrijemPonude;
    }

    public String getRokZaIzvrsenje() {
        return rokZaIzvrsenje;
    }

    public void setRokZaIzvrsenje(String rokZaIzvrsenje) {
        this.rokZaIzvrsenje = rokZaIzvrsenje;
    }

    public List<Ponuda> getListaPonuda() {
        return listaPonuda;
    }

    public void setListaPonuda(List<Ponuda> listaPonuda) {
        this.listaPonuda = listaPonuda;
    }
}
