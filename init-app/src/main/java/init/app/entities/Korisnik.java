package init.app.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Korisnik {
    @Id
    @GeneratedValue
    private long id;

    private String ime;
    private String korisnickoIme;
    private String lozinka;
    private String email;
    private String adresa;
    private String postanskiBroj;
    private String grad;
    private String tip;
    private boolean verifikovan;
    private double xKordinata;
    private double yKordinata;
    private double maxUdaljenost;

    public Korisnik(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getPostanskiBroj() {
        return postanskiBroj;
    }

    public void setPostanskiBroj(String postanskiBroj) {
        this.postanskiBroj = postanskiBroj;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public boolean isVerifikovan() {
        return verifikovan;
    }

    public void setVerifikovan(boolean verifikovan) {
        this.verifikovan = verifikovan;
    }


    public double getxKordinata() {
        return xKordinata;
    }

    public void setxKordinata(double xKordinata) {
        this.xKordinata = xKordinata;
    }

    public double getyKordinata() {
        return yKordinata;
    }

    public void setyKordinata(double yKordinata) {
        this.yKordinata = yKordinata;
    }

    public double getMaxUdaljenost() {
        return maxUdaljenost;
    }

    public void setMaxUdaljenost(double maxUdaljenost) {
        this.maxUdaljenost = maxUdaljenost;
    }
}
