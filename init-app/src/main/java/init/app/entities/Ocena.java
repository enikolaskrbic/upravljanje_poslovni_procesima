package init.app.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Ocena {
    @Id
    @GeneratedValue
    private Long id;
    private long ocena;
    private String korisnik;
    private String ocenjeniKorisnik;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getOcena() {
        return ocena;
    }

    public void setOcena(long ocena) {
        this.ocena = ocena;
    }

    public String getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(String korisnik) {
        this.korisnik = korisnik;
    }

    public String getOcenjeniKorisnik() {
        return ocenjeniKorisnik;
    }

    public void setOcenjeniKorisnik(String ocenjeniKorisnik) {
        this.ocenjeniKorisnik = ocenjeniKorisnik;
    }
}
