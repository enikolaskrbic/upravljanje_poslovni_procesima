package init.app.component;

import init.app.service.*;

public class StaticService {
    public static EmailService emailService;
    public static KategorijaService kategorijaService;
    public static KorisnikService korisnikService;
    public static KordinateService kordinateService;
    public static PonudaService ponudaService;
    public static ZahtevServis zahtevServis;
    public static OcenaServis ocenaServis;
    public static TaskService taskService;

}
