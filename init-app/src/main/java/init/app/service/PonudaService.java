package init.app.service;

import init.app.entities.Ponuda;
import init.app.repository.PonudaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PonudaService {
    
    @Autowired
    private PonudaRepository ponudaRepository;

    public Ponuda findOne(Long id){return ponudaRepository.findOne(id);}

    public List<Ponuda> findAll(){return ponudaRepository.findAll();}

    public Ponuda save(Ponuda offer){return ponudaRepository.save(offer);}

    //public void delete(Long id){ponudaRepository.delete(i);}
    public void  delete(Long ponuda) { ponudaRepository.delete(ponuda);}
}
