package init.app.service;

import init.app.entities.Zahtev;
import init.app.repository.ZahtevRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZahtevServis {
    @Autowired
    private ZahtevRepository zahtevRepository;

    public Zahtev findOne(Long id){return zahtevRepository.findOne(id);}

    public List<Zahtev> findAll(){return zahtevRepository.findAll();}

    public Zahtev save(Zahtev request){return zahtevRepository.save(request);}

    public void delete(Long id){zahtevRepository.delete(id);}
}
