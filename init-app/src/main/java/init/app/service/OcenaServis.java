package init.app.service;

import init.app.entities.Ocena;
import init.app.repository.OcenaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OcenaServis {

    @Autowired
    private OcenaRepository ocenaRepository;

    public Ocena findOne(Long id){return ocenaRepository.findOne(id);}

    public List<Ocena> findAll(){return ocenaRepository.findAll();}

    public Ocena save(Ocena rate){return ocenaRepository.save(rate);}

    public void delete(Long id){ocenaRepository.delete(id);}
}
