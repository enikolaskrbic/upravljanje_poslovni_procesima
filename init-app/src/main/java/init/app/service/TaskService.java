package init.app.service;

import init.app.entities.Kategorija;
import init.app.entities.Ponuda;
import init.app.service.activiti.AukcijaServis;
import init.app.web.dto.EnumDTO;
import init.app.web.dto.FormDTO;
import init.app.web.dto.TaskModelDTO;
import org.activiti.engine.FormService;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Bakir Niksic on 2/11/2018.
 */
@Service
public class TaskService {


    @Autowired
    org.activiti.engine.TaskService taskService;

    @Autowired
    FormService formService;

    @Autowired
    KategorijaService kategorijaService;

    @Autowired
    AukcijaServis aukcijaServis;

    public TaskService() {

    }

    public List<TaskModelDTO> getAllTasks(String assigner) {
        ArrayList<TaskModelDTO> retValue = new ArrayList<>();
        for (Task task : taskService.createTaskQuery().taskAssignee(assigner).list()) {
            retValue.add(new TaskModelDTO(task.getId(), task.getName()));
        }
        return retValue;
    }

    public List<FormDTO> getFormPropeties(String taskId) {
        ArrayList<FormDTO> retValue = new ArrayList<>();

        long zahtevId = 1;

        int i = 0;
        for (org.activiti.engine.form.FormProperty formPropertyInterface : formService.getTaskFormData(taskId).getFormProperties()) {
            if (formPropertyInterface.getId() == null)
                continue;
            if (formPropertyInterface.getId().contains("ponuda_zahtev")){
                try{
                    zahtevId = Long.parseLong(formPropertyInterface.getValue());
                }catch (Exception e){
                    zahtevId = 1;
                }
            }
            FormDTO myFormProperty = new FormDTO();
            myFormProperty.setLabel(formPropertyInterface.getName());
            myFormProperty.setName(formPropertyInterface.getId());
            myFormProperty.setValue(formPropertyInterface.getValue());
            System.out.println(i++ + " Possible: " + formPropertyInterface.getType());
            if (formPropertyInterface.getType().getName().equals("string")) {
                myFormProperty.setType("text");
            } else {
                myFormProperty.setType(formPropertyInterface.getType().getName());
            }
            myFormProperty.setReadable(formPropertyInterface.isReadable());
            myFormProperty.setWritable(formPropertyInterface.isWritable());
            myFormProperty.setRequired(formPropertyInterface.isRequired());
            if (formPropertyInterface.getType().getName().equals("enum")) {
                ArrayList<EnumDTO> arrayListString = new ArrayList<>();
                if (formPropertyInterface.getId().contains("rangListaPonuda")) {
                    List<Ponuda> ponudas = aukcijaServis.analizaPonuda(zahtevId);
                    for (Ponuda ponuda : ponudas) {
                        arrayListString.add(new EnumDTO(ponuda.getId().toString(), ponuda.getId().toString()));
                    }
                } else {
                    HashMap<String, String> hashMapString = (HashMap<String, String>) formPropertyInterface.getType().getInformation("values");
                    System.out.println("Values of enumaration: " + hashMapString.size());
                    for (String keyValue : hashMapString.keySet()) {
                        arrayListString.add(new EnumDTO(keyValue, hashMapString.get(keyValue)));
                    }
                }
                myFormProperty.setValues(arrayListString);
            }


//            if (formPropertyInterface.getId().startsWith("enum_")) {
//                myFormProperty.setType("enum");
//                ArrayList<EnumDTO> arrayListEnumItems = new ArrayList<>();
//                if (formPropertyInterface.getId().contains("enum_category")) {
//                    for (Kategorija kategorija : kategorijaService.findAll()) {
//                        if (kategorija.getKorisnickoIme() == null)
//                            arrayListEnumItems.add(new EnumDTO(kategorija.getActivitiId(), kategorija.getIme()));
//                    }
//                    myFormProperty.setValues(arrayListEnumItems);
//                }
//            }
            retValue.add(myFormProperty);
        }
        return retValue;
    }

    public void postFormPropetiesOnTaskAndExecute(String taskId, List<FormDTO> formPropetiesJson) {
        Map<String, String> submitData = new HashMap<String, String>();
        for (FormDTO formPropertyJson : formPropetiesJson) {
            if (formPropertyJson.isWritable() && !formPropertyJson.getName().contains("rangListaPonuda")) {
                if (formPropertyJson.getType().equals("enum")) {
                    for (int i = 0; i < formPropertyJson.getValues().size(); i++) {
                        if (formPropertyJson.getValues().get(i).getValue().equals(formPropertyJson.getValue())) {
                            submitData.put(formPropertyJson.getName(), formPropertyJson.getValues().get(i).getName());
                            break;
                        }
                    }
                } else {
                    submitData.put(formPropertyJson.getName(), formPropertyJson.getValue());
                }
            }
        }
        formService.submitTaskFormData(taskId, submitData);
    }
}
