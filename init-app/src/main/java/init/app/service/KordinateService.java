package init.app.service;

import init.app.entities.Kordinate;
import init.app.repository.KordinateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KordinateService {

    @Autowired
    KordinateRepository kordinateRepository;


    public Kordinate findOne(Long id){return kordinateRepository.findOne(id);}

    public List<Kordinate> findAll(){return kordinateRepository.findAll();}

    public List<Kordinate> findByUserName(String korisnickoIme){return kordinateRepository.findByUsername(korisnickoIme);}

    public Kordinate save(Kordinate kordinate){return kordinateRepository.save(kordinate);}

    public void delete(Long id){kordinateRepository.delete(id);}
}
