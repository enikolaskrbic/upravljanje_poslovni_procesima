package init.app.service.activiti;

import init.app.component.StaticService;
import init.app.entities.*;
import init.app.enumerations.FronConst;
import init.app.enumerations.PonudaConst;
import init.app.service.PonudaService;
import init.app.service.ZahtevServis;
import init.app.util.Ispis;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class AukcijaServis implements Serializable {


    public long sacuvajZahtev(long zahtevId,
                              String kategorija_posla,
                              String opis_posla,
                              long max_procenjena_vrednost,
                              String rok_za_prijem_ponude,
                              long max_br_ponuda,
                              String rok_za_izvrsenje_usluga) {
        Zahtev zahtev;
        if(zahtevId!=-1){
            zahtev = StaticService.zahtevServis.findOne(zahtevId);
            zahtev.setKategorija(kategorija_posla);
            zahtev.setOpisPosla(opis_posla);
            zahtev.setMaxCena(max_procenjena_vrednost);
            zahtev.setRokZaPrijemPonude(rok_za_prijem_ponude);
            zahtev.setMaxBrojPonuda(max_br_ponuda);
            zahtev.setRokZaIzvrsenje(rok_za_izvrsenje_usluga);
        }else {
            zahtev = new Zahtev(kategorija_posla, opis_posla, max_procenjena_vrednost, rok_za_prijem_ponude, max_br_ponuda, rok_za_izvrsenje_usluga);
        }
        StaticService.zahtevServis.save(zahtev);
        obrisiPonude(zahtevId);
        return zahtev.getId();
    }

    public List<Korisnik> listaKompanija(String kategorija_posla, String korisnik) {
        Korisnik korisnik1 = StaticService.korisnikService.findByUsername(korisnik).get(0);
        List<Kategorija> kategorijas = StaticService.kategorijaService.
                findAll().
                stream().
                filter(x ->
                        x.getIme().equals(kategorija_posla)
                                && x.getKorisnickoIme() != null
                ).
                collect(Collectors.toList());
        List<Korisnik> korisniks = new ArrayList<>();
        kategorijas.forEach(kategorija -> {
            Korisnik k = StaticService.korisnikService.findByUsername(kategorija.getKorisnickoIme()).get(0);
            if (korisnik1.getMaxUdaljenost() >= getAirDistance(korisnik1.getxKordinata(), korisnik1.getyKordinata(), k.getxKordinata(), k.getyKordinata())) {
                korisniks.add(k);
            }

        });
        return korisniks;
    }

    public void notifikacijaNemaKompanija(String korisnik) {
        StaticService.emailService.send(StaticService.korisnikService.findByUsername(korisnik).get(0).getEmail(),
                "No companies",
                "",
                "<html>" +
                        "<head></head>" +
                        "<body>" +
                        "<h2>No companies for request!</h2>" +
                        "<body>" +
                        "</html>");
    }

    public void notifikacijaImaManjeKompanija(String korisnik) {
        StaticService.emailService.send(StaticService.korisnikService.findByUsername(korisnik).get(0).getEmail(),
                "Lower comanies then requested",
                "",
                "<html>" +
                        "<head></head>" +
                        "<body>" +
                        "<h2>Do you wanna sent mail to other comapnies?</h2>" +
                        "<body>" +
                        "</html>");
    }

    public void notifikacijaKompanijeZaPonudu(String email) {
        StaticService.emailService.send(email,
                "You have new offer",
                "",
                "<html>" +
                        "<head></head>" +
                        "<body>" +
                        "<h2>New offer!</h2>" +
                        "<body>" +
                        "</html>");
    }

    public long sacuvajPonudu(long ponudaId, long zahtevId, String korisnickoIme, long cena, String datumPonude) {
        Ponuda ponuda;
        if (ponudaId == -1) {
            ponuda = new Ponuda(cena,
                    datumPonude,
                    StaticService.zahtevServis.findOne(zahtevId),
                    PonudaConst.PONUDA_NEPRIHVACENA,
                    korisnickoIme);
        } else {
            ponuda = StaticService.ponudaService.findOne(ponudaId);
            ponuda.setCena(cena);
            ponuda.setDatum(datumPonude);
        }
        StaticService.ponudaService.save(ponuda);
        prikazRangListe(zahtevId,cena,datumPonude,StaticService.korisnikService.findByUsername(korisnickoIme).get(0).getEmail());
        return ponuda.getId();
    }

    public void prikazRangListe(long zahtevId, long cena, String datumPonude, String email) {
        List<Ponuda> ponudas = StaticService.ponudaService.findAll();
        Collections.shuffle(ponudas);
        StaticService.emailService.send(email,
                "Rank list:",
                "",
                "<html>" +
                        "<head></head>" +
                        "<body>" +
                        "<h2>Rang lista:!</h2><br>" +
                        "<p>" + analizaPonudaToString(ponudas) + "</p>" +
                        "<body>" +
                        "</html>");
    }

    public List<Ponuda> analizaPonuda(long zahtevId) {
        return StaticService.ponudaService
                .findAll()
                .stream()
                .filter(x -> x.getZahtev().getId() == zahtevId)
                .sorted((o1, o2) -> o1.getCena() > o2.getCena() ? 1 : 0)
                .collect(Collectors.toList());
    }

    public void notofikujPreProduzetkaRoka(long zahtevId, String korisnik) {
        Korisnik korisnik1 = StaticService.korisnikService.findByUsername(korisnik).get(0);
        StaticService.emailService.send(korisnik1.getEmail(),
                "New deadline?",
                "",
                "<html>" +
                        "<head></head>" +
                        "<body>" +
                        "<p>New deadline for request? " + zahtevId + "</p>" +
                        "<body>" +
                        "</html>");
    }

    public List<Ponuda> rangirajPonude(String korisnickoIme,long zahtevId) {
        String ranglista = analizaPonudaToString(analizaPonuda(zahtevId));
        StaticService.emailService.send(StaticService.korisnikService.findByUsername(korisnickoIme).get(0).getEmail(),
                "Rang list",
                "",
                "<html>" +
                        "<head></head>" +
                        "<body>" +
                        "<p>Rang list:  " + ranglista + "</p>" +
                        "<body>" +
                        "</html>");
        return analizaPonuda(zahtevId);
    }

    private String analizaPonudaToString(List<Ponuda> ponudas) {
        String rangLista ="";
        for (Ponuda ponuda : ponudas){
            rangLista+=ponuda.toString();
        }
        return rangLista;
    }

    public String zadajNoviRok(long zahtevId, String novi_rok_ponude) {
        Zahtev zahtev = StaticService.zahtevServis.findOne(zahtevId);
        zahtev.setRokZaPrijemPonude(novi_rok_ponude);
        StaticService.zahtevServis.save(zahtev);
        return novi_rok_ponude;
    }

    public void potvrdiPonudu(long ponudaId) {
        Ponuda ponuda = StaticService.ponudaService.findOne(ponudaId);
        ponuda.setStatus(PonudaConst.PONUDA_PRIHVACENA);
        StaticService.ponudaService.save(ponuda);
    }

    public List<Korisnik> listaKompanijaSaNovimRokom(long zahtevId, List<Korisnik> listaKompanija) {
        List<Ponuda> ponudas = StaticService.ponudaService.findAll().stream().filter(x->x.getZahtev().getId() == zahtevId).collect(Collectors.toList());
        List<Korisnik> kompanije = new ArrayList<>();
        boolean flag = false;
        for (Korisnik korisnik: listaKompanija) {
            for (Ponuda ponuda: ponudas) {
                if(ponuda.getKorisnickoIme().equals(korisnik.getKorisnickoIme())){
                    flag = true;
                    break;
                }
            }
            if(!flag){
                kompanije.add(korisnik);
            }
        }
        return kompanije;
    }

    public void obrisiPonude(long zahtevId) {
        List<Long> ponudas = StaticService.ponudaService.findAll().stream().filter(x->x.getZahtev().getId() == zahtevId).map(Ponuda::getId).collect(Collectors.toList());
        for (Long ponuda: ponudas) {
            StaticService.ponudaService.delete(ponuda);
        }
    }

    public long uvecajBrojPonavljanja(long brojPonavljanjaProcesa) {
        brojPonavljanjaProcesa++;
        return brojPonavljanjaProcesa;
    }

    public String pronadjiKompaniju(long izabranaPonuda) {
        Ponuda ponudas = StaticService.ponudaService.findOne(izabranaPonuda);
        return ponudas.getKorisnickoIme();
    }


    public void traziDetaljeOdKompanije(long izabranaPonuda, String pronadjenaKompanija) {
        Korisnik korisnik = StaticService.korisnikService.findByUsername(pronadjenaKompanija).get(0);
        StaticService.emailService.send(korisnik.getEmail(),
                "More details about offer",
                "",
                "<html>" +
                        "<head></head>" +
                        "<body>" +
                        "<p>Add details about offer:  " + izabranaPonuda + "</p>" +
                        "<body>" +
                        "</html>");
    }

    public void viseDetaljaOdKompanije(String korisnik, String viseDetaljaOPonudi) {
        Korisnik korisnik1 = StaticService.korisnikService.findByUsername(korisnik).get(0);
        StaticService.emailService.send(korisnik1.getEmail(),
                "More details about offer",
                "",
                "<html>" +
                        "<head></head>" +
                        "<body>" +
                        "<h3>More details: </h3><br>" +
                        "<p> " + viseDetaljaOPonudi + ":</p><br>" +
                        "<body>" +
                        "</html>");
    }


    public void sacuvajOcene(String korisnik, long ocenaKlijenta, String pronadjenaKompanija, long ocenaKompanije) {
        Ocena ocena = new Ocena();
        ocena.setKorisnik(korisnik);
        ocena.setOcena(ocenaKlijenta);
        ocena.setOcenjeniKorisnik(pronadjenaKompanija);
        StaticService.ocenaServis.save(ocena);

        Ocena ocena2 = new Ocena();
        ocena2.setKorisnik(pronadjenaKompanija);
        ocena2.setOcena(ocenaKompanije);
        ocena2.setOcenjeniKorisnik(korisnik);
        StaticService.ocenaServis.save(ocena2);
    }

    public void obavestiKompanijuOPotvrdi(String pronadjenaKompanija,long izabranaPonuda){
        Korisnik korisnik1 = StaticService.korisnikService.findByUsername(pronadjenaKompanija).get(0);
        StaticService.emailService.send(korisnik1.getEmail(),
                "Offer is accepted",
                "",
                "<html>" +
                        "<head></head>" +
                        "<body>" +
                        "<p>Offer: " + izabranaPonuda + ":</p>" +
                        "<p> " + StaticService.ponudaService.findOne(izabranaPonuda).toString() + ":</p><br>" +
                        "<body>" +
                        "</html>");
    }

    /*
        Get air distance between two points (km)
     */
    public double getAirDistance(double fromX, double fromY, double toX, double toY) {

        final double earthRadius = 6371.0; // 6371.0 kilometers
        double dLat = Math.toRadians(toX - fromX);
        double dLng = Math.toRadians(toY - fromY);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(fromX)) * Math.cos(Math.toRadians(fromX));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return earthRadius * c;
    }
}
