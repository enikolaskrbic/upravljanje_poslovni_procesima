package init.app.service.activiti;

import com.google.maps.model.LatLng;
import init.app.component.StaticService;
import init.app.entities.Kategorija;
import init.app.entities.Kordinate;
import init.app.entities.Korisnik;
import init.app.enumerations.FronConst;
import init.app.enumerations.KorisnikConst;
import init.app.service.EmailService;
import init.app.service.KategorijaService;
import init.app.service.KordinateService;
import init.app.service.KorisnikService;
import init.app.util.GoogleUtil;
import init.app.util.Ispis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public class RegistracijaServis implements Serializable{



    public boolean proveriPodatke(String email,String korisnickoIme){
        Ispis.ispis("proveri Podatke...");
        return StaticService.korisnikService.isValid(korisnickoIme,email);
    }

    public void sacuvajKordinate(String adresa,String grad,String postanskiBroj,String korisnickoIme){
        Kordinate kordinate = new Kordinate();
        kordinate.setGrad(grad);
        kordinate.setKorisnickoIme(korisnickoIme);
        LatLng latLng = GoogleUtil.kordinateOdGrada(adresa,grad,postanskiBroj);
        kordinate.setxKordinata(latLng.lat);
        kordinate.setyKordinata(latLng.lng);
        StaticService.kordinateService.save(kordinate);
    }

    public void dodajKategorije(String multi_kategorije,String korisnickoIme){
        Ispis.ispis("dodaj kategoriju...");
        String kategorije[] = multi_kategorije.split(";");
        for (String imeKategorije : kategorije){
            Kategorija kategorija1 = new Kategorija();
            kategorija1.setIme(imeKategorije);
            kategorija1.setOpis(imeKategorije);
            kategorija1.setKorisnickoIme(korisnickoIme);
            StaticService.kategorijaService.save(kategorija1);
        }
    }

    public long sacuvajKorisnikaPrivremeno(String ime,String korisnickoIme,String lozinka,String email,String adresa,String postanskiBroj,String grad,String tip, long maxUdaljenost,String logKorisnik){
        Ispis.ispis("sacuvaj korisnika privremeno");
        Korisnik korisnik = new Korisnik();
        korisnik.setVerifikovan(false);
        korisnik.setAdresa(adresa);
        korisnik.setEmail(email);
        korisnik.setGrad(grad);
        korisnik.setIme(ime);
        korisnik.setKorisnickoIme(korisnickoIme);
        korisnik.setLozinka(lozinka);
        korisnik.setPostanskiBroj(postanskiBroj);
        korisnik.setTip(tip);
        korisnik.setMaxUdaljenost(maxUdaljenost);
        if(tip.equals(KorisnikConst.KOMPANIJA)){
            Kordinate kordinate = StaticService.kordinateService.findByUserName(korisnickoIme).get(0);
            korisnik.setxKordinata(kordinate==null ? 0L : kordinate.getxKordinata());
            korisnik.setyKordinata(kordinate==null ? 0L : kordinate.getyKordinata());
        }

        StaticService.korisnikService.save(korisnik);
        posaljiMail(korisnik.getEmail(),logKorisnik);
        return korisnik.getId();
    }

    private void posaljiMail(String email,String logKorisnik){
        Ispis.ispis("posalji mail");
        StaticService.emailService.send(email,
                "Verifikacija",
                "You need to verify your account! Please click: http://localhost:4200/tasks?user=" + logKorisnik,
                "<html>" +
                             "<head></head>" +
                                "<body> <h3>You need to verify your account! Please click here:</h3>" +
                                    "<a href=\"http://localhost:"+FronConst.FRONT_PORT+"/?#/submit-mail?user=" + logKorisnik+ "\" >Verificate</a>"+
                                "<body>"+
                            "</html>");
    }

    public void sacuvajKorisnika(long korisnikId){
        Ispis.ispis("sacuvaj korisnika");
        Korisnik korisnik = StaticService.korisnikService.findOne(korisnikId);
        korisnik.setVerifikovan(true);
        StaticService.korisnikService.save(korisnik);
    }

    public void obrisiKorisnika(long korisnikId){
        Ispis.ispis("obrisi korisnika");
        StaticService.korisnikService.delete(korisnikId);
    }




}
