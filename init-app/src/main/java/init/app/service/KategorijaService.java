package init.app.service;

import init.app.entities.Kategorija;
import init.app.repository.KategorijaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KategorijaService {

    @Autowired
    KategorijaRepository kategorijaRepository;


    public Kategorija findOne(Long id){return kategorijaRepository.findOne(id);}

    public List<Kategorija> findAll(){return kategorijaRepository.findAll();}

    public Kategorija save(Kategorija category){return kategorijaRepository.save(category);}

    public void delete(Long id){kategorijaRepository.delete(id);}
}
