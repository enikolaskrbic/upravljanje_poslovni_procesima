package init.app.service;

import init.app.entities.Korisnik;
import init.app.repository.KorisnikRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class KorisnikService {

    @Autowired
    KorisnikRepository korisnikRepository;

    public Korisnik findOne(Long id){return korisnikRepository.findOne(id);}

    public Korisnik save(Korisnik korisnik){
        return korisnikRepository.save(korisnik);
    }

    public void delete(Long id){korisnikRepository.delete(id);}

    public List<Korisnik> findAll(){return korisnikRepository.findAll();}

    public boolean isValid(String korisnickoIme, String email){
        return  korisnikRepository.findByUsernameAndMail(korisnickoIme,email).size()==0;
    }

    public boolean doesUserExist(String korisnickoIme, String lozinka){
        return korisnikRepository.findByUsernameAndPassword(korisnickoIme, lozinka).size() == 1;
    }

    public List<Korisnik> findByUsername(String username){
        return korisnikRepository.findByUsername(username);
    }
}
