package init.app.service;

import org.apache.commons.lang3.CharEncoding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendMail(String mailTo){
        try {
            SimpleMailMessage emailSMM = new SimpleMailMessage();
            emailSMM.setFrom("uppftn@gmail.com");
            emailSMM.setTo(mailTo);
            emailSMM.setSubject("Test");
            emailSMM.setText("Testing");
            javaMailSender.send(emailSMM);
        } catch (Exception ex) {
            System.out.println("Email is not sent." + ex.getMessage());
        }
    }

    public void sendMail(String mailTo, String subject, String text){
        try {
            SimpleMailMessage emailSMM = new SimpleMailMessage();
            emailSMM.setFrom("uppftn@gmail.com");
            emailSMM.setTo(mailTo);
            emailSMM.setSubject(subject);
            emailSMM.setText(text);
            javaMailSender.send(emailSMM);
            System.err.println("EMAIL SENT TO : "+ mailTo);
        } catch (Exception ex) {
            System.out.println("Email is not sent." + ex.getMessage());
        }
    }

    @Async
    public void send(String to, String subject, String plainTextContent, String htmlContent) {
        final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, CharEncoding.UTF_8);
            message.setTo(to);
            message.setFrom("uppnikola@gmail.com");
            message.setReplyTo("uppnikola@gmail.com");
            message.setSubject(subject);
            message.setText(plainTextContent, htmlContent);

            javaMailSender.send(mimeMessage);
        } catch (Throwable e) {
            System.out.println("Email is not sent." + e.getMessage());
        }
    }


}

